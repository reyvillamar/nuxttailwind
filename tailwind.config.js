module.exports = {
  variants: {},
  plugins: [],
  content: [
    `components/**/*.{vue,js}`,
    `layouts/**/*.vue`,
    `pages/**/*.vue`,
    `plugins/**/*.{js,ts}`,
    `nuxt.config.{js,ts}`
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        transparent: 'transparent',
        current: 'currentColor',
        'primary': '#0e8c8b',
        'secondary': '#1f77c3',
        'success': '#e2fafb',
        'active': '#81E6D9',
        'dark-gray': '#4A5568',
        'light-gray': '#718096'
      }
    }
  }
}
